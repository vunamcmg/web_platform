import { ViewRouter } from './view.router'
import { ApiRouter } from './api.router'

const view: any = (new ViewRouter()).router
const api: any = (new ApiRouter()).router

export {
    view, api
}