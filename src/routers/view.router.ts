import { BaseRouter, Request, Response } from "./base.router";
import * as express from 'express'
import * as path from 'path'
import { appCoreService, blogCoreService, contactCoreService, postCoreService, serviceCoreService, teamCoreService, pageCoreService } from "../services";
import { config } from "../config";

export class ViewRouter extends BaseRouter {
    constructor() {
        super()
        this.router = express.Router();
        this.routing()
    }
    router: express.Router
    routing() {
        // Trang chủ
        this.router.get("/", this.route(this.renderHome))
        this.router.get("/index", this.route(this.renderHome))

        // Blog
        this.router.get("/bai-viet", this.route(this.renderBlog))
        this.router.get("/blog", this.route(this.renderBlog))
        this.router.get("/bai-viet/:categorySlug", this.route(this.renderPostCategory))
        this.router.get("/bai-viet/:categorySlug/:slug", this.route(this.renderPost))
        this.router.get("/post", this.route(this.renderPost))
        this.router.post("/comment", this.route(this.createComment))

        // Liên hệ
        this.router.get("/lien-he", this.route(this.renderContact))
        this.router.get("/contact", this.route(this.renderContact))
        this.router.post("/lien-he", this.route(this.createContact))
        this.router.post("/contact", this.route(this.createContact))

        // Giới thiệu
        this.router.get("/gioi-thieu", this.route(this.renderAbout))
        this.router.get("/about", this.route(this.renderAbout))

        // Dịch vụ
        this.router.get("/dich-vu", this.route(this.renderServices))
        this.router.get("/services", this.route(this.renderServices))

        this.router.get("/dich-vu/:slug", this.route(this.renderServiceDetail))
        this.router.get("/services/:slug", this.route(this.renderServiceDetail))

        // Đội ngũ
        this.router.get("/doi-ngu", this.route(this.renderTeam))
        this.router.get("/team", this.route(this.renderTeam))
        this.router.get("/doi-ngu/:slug", this.route(this.renderTeamDetail))
        this.router.get("/team/:slug", this.route(this.renderTeamDetail))

        // Sự kiện
        this.router.get("/su-kien", this.route(this.renderEvents))
        this.router.get("/events", this.route(this.renderEvents))
        this.router.get("/su-kien/:slug", this.route(this.renderEventsDetail))
        this.router.get("/events/:slug", this.route(this.renderEventsDetail))

        // Time line
        this.router.get("/hanh-trinh", this.route(this.renderTimeline))
        this.router.get("/timeline", this.route(this.renderTimeline))

        // Thương mại điện tử
        this.router.get("/cua-hang", this.route(this.renderShop))
        this.router.get("/cua-hang/:categorySlug", this.route(this.renderProductCategory))
        this.router.get("/cua-hang/:categorySlug/:productSlug", this.route(this.renderProduct))
        this.router.get("/gio-hang", this.route(this.renderCart))
        this.router.get("/dat-hang", this.route(this.renderCheckoutProcess))
        this.router.get("/dat-hang-thanh-cong", this.route(this.renderCheckoutSuccess))

        // Tài khoản
        this.router.get("/dang-nhap", this.route(this.renderLogin))
        this.router.get("/dang-ky", this.route(this.renderSignUp))
        this.router.get("/tai-khoan", this.route(this.renderProfile))

        // Lỗi
        this.router.get("/404", this.route(this.renderError))


        //this.router.get(/(^[A-Za-z][^.]*$)|^[A-Za-z]*$/, this.route(this.renderDynamic))
        this.router.get(/^[^api][A-Za-z0-9][^.]*$/, (function (req, res, next) {
            if (req.path.includes("api")) {
                next()
            } else {
                let filePath = req.path[req.path.length - 1] === "/" ? req.path.slice(0, req.path.length - 1) : req.path
                const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
                res.render(path.join(__dirname, `../../views${filePath}`), { setting })
            }
        }))
        //this.router.get(/^\/api\/+$/, this.route(this.renderDynamic))
    }
    async renderLogin(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/login`), { setting })
    }
    async renderSignUp(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/signup`), { setting })
    }
    async renderProfile(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/profile`), { setting })
    }

    async renderShop(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/shop`), { setting })
    }
    async renderProductCategory(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/productCategory`), { setting })
    }
    async renderProduct(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/product`), { setting })
    }
    async renderCart(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/cart`), { setting })
    }
    async renderCheckoutProcess(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/checkoutProcess`), { setting })
    }
    async renderCheckoutSuccess(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/checkoutSuccess`), { setting })
    }
    async renderDynamic(req: Request, res: Response) {
        let filePath = req.path[req.path.length - 1] === "/" ? req.path.slice(0, req.path.length - 1) : req.path
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views${filePath}`), { setting })
    }
    async renderHome(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "home"
            },
            fields: ["title", "metaTitle", "metaDescription", "metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views`), { pageData, setting })
    }
    async renderBlog(req: Request, res: Response) {
        const blogData = await blogCoreService.getList({
            fields: ["$all"],
            limit: 5,
            offset: req.query.page * 5 || 0
        })
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "blog"
            },
            fields: ["title", "metaTitle", "metaDescription", "metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/blog`), { pageData, blogs: blogData.rows, pagination: blogData.pagination, setting })
    }
    async renderPostCategory(req: Request, res: Response) {
        const post = await postCoreService.getItemBySlug(req.params.slug, {
            fields: ["$all"]
        })
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "post_category"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/category`), { pageData, post, setting })
    }
    async renderPost(req: Request, res: Response) {
        const post = await postCoreService.getItemBySlug(req.params.slug, {
            fields: ["$all"]
        })
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "post"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/post`), { pageData, post, setting })
    }

    async renderContact(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "contact"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/contact`), { pageData, setting })
    }

    async createContact(req: Request, res: Response) {
        await contactCoreService.create(req.body)
        res.status(200).json(req.body)
    }

    async renderAbout(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "about"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/about`), { pageData, setting })
    }
    async renderServices(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "service"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const services = await serviceCoreService.getList({
            fields: ["$all"]
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/services`), { pageData, services, setting })
    }

    async renderServiceDetail(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "service_detail"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const service = await serviceCoreService.getItemBySlug(req.params.slug, {
            fields: ["$all"]
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/service`), { pageData, service, setting })
    }

    async renderEvents(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "event"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/events`), { pageData, setting })
    }

    async renderEventsDetail(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "event_detail"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/eventDetail`), { pageData, setting })
    }

    async renderTeam(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "team"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const teams = await teamCoreService.getList({
            fields: ["$all"]
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/team`), { pageData, teams, setting })
    }
    async renderTeamDetail(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "team_detail"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const team = await teamCoreService.getItemBySlug(req.params.slug, {
            fields: ["$all"]
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/teamDetail`), { pageData, team, setting })
    }

    async renderTimeline(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/timeline`), { setting })
    }
    async renderError(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/error`), { setting })
    }

    async createComment(req: Request, res: Response) {
        res.status(200).json(req.body)
    }

}

