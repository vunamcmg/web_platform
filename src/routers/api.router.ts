import { BaseRouter, Request, Response } from "./base.router";
import * as express from 'express'
import * as path from 'path'
import { appCoreService, blogCoreService } from "../services";
import { config } from "../config";
import * as fs from 'fs'
import * as _ from 'lodash'
import * as dirTree from 'directory-tree'

export class ApiRouter extends BaseRouter {
    constructor() {
        super()
        this.router = express.Router();
        this.routing()
    }
    router: express.Router
    routing() {
        this.router.get("/setting", this.route(this.getSetting))
        this.router.post("/setting", this.route(this.updateSetting))
        this.router.get("/page", this.route(this.getPage))
        this.router.get("/page/raw", this.route(this.getPageRaw))
        this.router.post("/page/raw", this.route(this.updatePageRaw))
    }
    async getSetting(req: Request, res: Response) {
        const token = req.query.token
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/setting/setting`), { setting })
    }
    async updateSetting(req: Request, res: Response) {
        const token = req.query.token
        fs.writeFileSync(path.join(__dirname, `../../views/setting/setting.json`), JSON.stringify(req.body))
        res.render(path.join(__dirname, `../../views/setting/setting`), { setting: req.body })
    }
    async getPage(req: Request, res: Response) {
        const token = req.query.token

        const tree = dirTree(path.join(__dirname, "../../views"))
        res.status(200).json(tree)
    }

    async getPageRaw(req: Request, res: Response) {
        const token = req.query.token
        const fileRaw = fs.readFileSync(req.query.path, "utf8")
        res.status(200).json({
            path: req.query.path,
            content: fileRaw
        })
    }
    async updatePageRaw(req: Request, res: Response) {
        const token = req.query.token
        const content = req.body.content
        console.log("content: ", req.body)
        // res.status(200).json({
        //     path: "q2121"
        // })
        const fileRaw = fs.writeFileSync(req.query.path, content)
        res.status(200).json({
            path: req.query.path,
            content: fileRaw
        })
    }
    // walkSync(dir, filelist) {
    //     var files = fs.readdirSync(dir);
    //     for (const file of files) {
    //         console.log(`${file}`, fs.statSync(path.join(dir, `/${file}`)).isDirectory())
    //         if (fs.statSync(path.join(dir, `/${file}`)).isDirectory()) {
    //             filelist = this.walkSync(path.join(dir, `/${file}`), filelist)
    //         } else {
    //             filelist.push(`${dir}/${file}`)
    //         }
    //     }
    //     return filelist
    // };
}

