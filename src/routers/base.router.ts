import * as express from 'express';
import * as _ from 'lodash';
import { config } from '../config'

export interface Request extends express.Request {
    [x: string]: any
    firebaseUserInfo: any
}
export interface Response extends express.Response {
    [x: string]: any
}
export interface IValidateSchema {
    type?: string | string[]
    properties?: IValidateSchemaProperties
    additionalProperties?: boolean
    required?: string[]
    uniqueItems?: boolean
    minItems?: number
    items?: IValidateSchema
    [x: string]: any
}
export interface IValidateSchemaProperties {
    [x: string]: IValidateSchema
}
export class BaseRouter {

    onError(res: Response, error: any) {
        console.log("err: ", error)
        res.send(error)
        // const options = error.options || error.errorInfo;
        res.redirect("/404")
    }
    onSuccess(res: Response, object: any = {}, extras: any = {}) {
        
    }
    
    async validateJSON(body: any, schema: IValidateSchema) {
        // const validate = utilService.validateJSON(schema, body)
        // if (!validate.isValid) {
        //     throw errorService.router.requestDataInvalid(validate.message);
        // }
    }
    route(func: (req: Request, rep: Response) => Promise<any>) {
        return (req: Request, res: Response) => func
            .bind(this)(req, res)
            .catch((error: any) => {
                this.onError(res, error)
            })
    }
}