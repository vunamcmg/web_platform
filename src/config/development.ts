import * as dotenv from 'dotenv'
dotenv.config({ silent: true })
export default {
    server: {
        host: 'localhost',
        protocol: 'http',
        debug: true,
        port: process.env.PORT || 3000,
        uiHost: 'http://localhost:3000',
        serverHost: "http://localhost:4000",
        version: 'v1',
    },
    database: {
        //mongo: process.env.MONGOLOCAL_URI,
        mongo: process.env.MONGOLOCAL_URI,
        defaultPageSize: 50,
    },
    app: {
        _id: "e3ec72a2-6148-4883-a742-42c2ab52e5c9",
        token: ""
    }


}