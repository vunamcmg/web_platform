import { BaseCoreService } from './base.service'


export class PageCoreService extends BaseCoreService {
    constructor() {
        super("page")
    }
}