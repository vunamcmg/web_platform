import { BaseCoreService } from './base.service'


export class BlogCoreService extends BaseCoreService {
    constructor() {
        super("blog")
    }
}