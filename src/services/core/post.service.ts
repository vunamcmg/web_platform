import { BaseCoreService } from './base.service'


export class PostCoreService extends BaseCoreService {
    constructor() {
        super("post")
    }
}