import { BaseCoreService } from './base.service'


export class ServiceCoreService extends BaseCoreService {
    constructor() {
        super("service")
    }
}