import { BaseCoreService } from './base.service'


export class AppCoreService extends BaseCoreService {
    constructor() {
        super("app")
    }
}