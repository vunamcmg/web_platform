import { BaseCoreService } from './base.service'


export class ProjectCoreService extends BaseCoreService {
    constructor() {
        super("project")
    }
}