import { BaseCoreService } from './base.service'


export class CommentCoreService extends BaseCoreService {
    constructor() {
        super("comment")
    }
}