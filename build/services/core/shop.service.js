"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class ShopCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("shop");
    }
}
exports.ShopCoreService = ShopCoreService;
//# sourceMappingURL=shop.service.js.map