"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class AppCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("app");
    }
}
exports.AppCoreService = AppCoreService;
//# sourceMappingURL=app.service.js.map