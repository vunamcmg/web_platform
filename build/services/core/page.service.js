"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class PageCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("page");
    }
}
exports.PageCoreService = PageCoreService;
//# sourceMappingURL=page.service.js.map