"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class ProjectCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("project");
    }
}
exports.ProjectCoreService = ProjectCoreService;
//# sourceMappingURL=project.service.js.map