"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class CommentCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("comment");
    }
}
exports.CommentCoreService = CommentCoreService;
//# sourceMappingURL=comment.service.js.map