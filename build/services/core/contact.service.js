"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class ContactCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("contact");
    }
}
exports.ContactCoreService = ContactCoreService;
//# sourceMappingURL=contact.service.js.map