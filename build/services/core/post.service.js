"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class PostCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("post");
    }
}
exports.PostCoreService = PostCoreService;
//# sourceMappingURL=post.service.js.map