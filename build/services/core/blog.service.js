"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class BlogCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("blog");
    }
}
exports.BlogCoreService = BlogCoreService;
//# sourceMappingURL=blog.service.js.map