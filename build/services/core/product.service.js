"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class ProductCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("product");
    }
}
exports.ProductCoreService = ProductCoreService;
//# sourceMappingURL=product.service.js.map