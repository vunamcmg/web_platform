"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const Request = require("request-promise");
const config_1 = require("../../config");
class BaseCoreService {
    constructor(path) {
        this.path = path;
    }
    baseUrl(subpath = "") {
        return `${config_1.config.server.serverHost}/api/v1/${this.path}/${subpath}`;
    }
    _paserQuery(query = {}) {
        const parsedQuery = _.merge({}, query);
        if (query.filter) {
            parsedQuery.filter = (JSON.stringify(query.filter));
        }
        if (query.order) {
            parsedQuery.order = (JSON.stringify(query.order));
        }
        if (query.scopes) {
            parsedQuery.scopes = (JSON.stringify(query.scopes));
        }
        if (query.fields) {
            parsedQuery.fields = (JSON.stringify(query.fields));
        }
        if (query.items) {
            parsedQuery.items = (JSON.stringify(query.items));
        }
        return parsedQuery;
    }
    async exec(promise) {
        try {
            return await promise;
        }
        catch (error) {
            //throw error
        }
    }
    async find(query = {}) {
        const options = {
            uri: this.baseUrl("find"),
            method: "GET",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'app_id': config_1.config.app._id,
                'app_token': config_1.config.app.token
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object;
        }
        else {
            return {};
        }
    }
    async getItem(itemId, query = {}) {
        const options = {
            uri: this.baseUrl(itemId),
            method: "GET",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'app_id': config_1.config.app._id,
                'app_token': config_1.config.app.token
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object;
        }
        else {
            return {};
        }
    }
    async getItemBySlug(slug, query = {}) {
        query.filter ? query.filter.slug = slug : query.filter = { slug: slug };
        const options = {
            uri: this.baseUrl("find"),
            method: "GET",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'app_id': config_1.config.app._id,
                'app_token': config_1.config.app.token
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object;
        }
        else {
            return {};
        }
    }
    async getList(query = {}) {
        const options = {
            uri: this.baseUrl(),
            method: "GET",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'app_id': config_1.config.app._id,
                'app_token': config_1.config.app.token
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.results && res.results.objects) {
            return Object.assign(Object.assign({}, res.results.objects), { pagination: res.pagination });
        }
        else {
            return {};
        }
    }
    async delete(itemId, query = {}) {
        const options = {
            uri: this.baseUrl(itemId),
            method: "DELETE",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'app_id': config_1.config.app._id,
                'app_token': config_1.config.app.token
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object;
        }
        else {
            return {};
        }
    }
    async update(itemId, body, query = {}) {
        const options = {
            uri: this.baseUrl(itemId),
            method: "PUT",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/x-www-form-urlencoded',
                'app_id': config_1.config.app._id,
                'app_token': config_1.config.app.token
            },
            body: body,
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object;
        }
        else {
            return {};
        }
    }
    async create(body, query = {}) {
        const options = {
            uri: this.baseUrl(),
            method: "POST",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'app_id': config_1.config.app._id,
                'app_token': config_1.config.app.token
            },
            body: body,
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object;
        }
        else {
            return {};
        }
    }
}
exports.BaseCoreService = BaseCoreService;
//# sourceMappingURL=base.service.js.map