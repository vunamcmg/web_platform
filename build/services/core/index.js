"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const about_service_1 = require("./about.service");
const app_service_1 = require("./app.service");
const blog_service_1 = require("./blog.service");
const post_service_1 = require("./post.service");
const product_service_1 = require("./product.service");
const project_service_1 = require("./project.service");
const shop_service_1 = require("./shop.service");
const comment_service_1 = require("./comment.service");
const contact_service_1 = require("./contact.service");
const service_service_1 = require("./service.service");
const team_service_1 = require("./team.service");
const tesmonial_service_1 = require("./tesmonial.service");
const page_service_1 = require("./page.service");
const aboutCoreService = new about_service_1.AboutCoreService();
exports.aboutCoreService = aboutCoreService;
const appCoreService = new app_service_1.AppCoreService();
exports.appCoreService = appCoreService;
const blogCoreService = new blog_service_1.BlogCoreService();
exports.blogCoreService = blogCoreService;
const postCoreService = new post_service_1.PostCoreService();
exports.postCoreService = postCoreService;
const productCoreService = new product_service_1.ProductCoreService();
exports.productCoreService = productCoreService;
const projectCoreService = new project_service_1.ProjectCoreService();
exports.projectCoreService = projectCoreService;
const shopCoreService = new shop_service_1.ShopCoreService();
exports.shopCoreService = shopCoreService;
const commentCoreService = new comment_service_1.CommentCoreService();
exports.commentCoreService = commentCoreService;
const contactCoreService = new contact_service_1.ContactCoreService();
exports.contactCoreService = contactCoreService;
const serviceCoreService = new service_service_1.ServiceCoreService();
exports.serviceCoreService = serviceCoreService;
const teamCoreService = new team_service_1.TeamCoreService();
exports.teamCoreService = teamCoreService;
const tesmonialCoreService = new tesmonial_service_1.TesmonialCoreService();
exports.tesmonialCoreService = tesmonialCoreService;
const pageCoreService = new page_service_1.PageCoreService();
exports.pageCoreService = pageCoreService;
//# sourceMappingURL=index.js.map