"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class ServiceCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("service");
    }
}
exports.ServiceCoreService = ServiceCoreService;
//# sourceMappingURL=service.service.js.map