"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = require("./base.service");
class AboutCoreService extends base_service_1.BaseCoreService {
    constructor() {
        super("about");
    }
}
exports.AboutCoreService = AboutCoreService;
//# sourceMappingURL=about.service.js.map