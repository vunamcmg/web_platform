"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const config_1 = require("./config");
const routers_1 = require("./routers");
class Server {
    constructor() {
        this.port = process.env.PORT || 5000;
        this.server = express();
        // this.initDB();
        this.initMiddlewares();
        this.initView();
        this.initRoute();
        this.initStatisFolder();
        this.initServer();
    }
    async initDB() {
        mongoose.connect(config_1.config.database.mongo, {
            useNewUrlParser: true,
            useFindAndModify: false
        });
    }
    async initMiddlewares() {
        this.server.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
        this.server.use(bodyParser.urlencoded({ extended: false }));
        this.server.use(bodyParser.json());
        this.server.use(cors());
    }
    async initRoute() {
        this.server.use('/api', routers_1.api);
    }
    async initStatisFolder() {
        this.server.set('view engine', 'ejs');
        //this.server.use(express.static(__dirname + `../views`));
        this.server.use(express.static('views'));
    }
    async initView() {
        this.server.use(routers_1.view);
    }
    async initServer() {
        this.server.listen(this.port, (err) => {
            console.log('Server listening on port ', this.port);
        });
    }
}
const server = new Server();
//# sourceMappingURL=app.js.map