"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
dotenv.config({ silent: true });
exports.default = {
    server: {
        host: 'localhost',
        protocol: 'http',
        debug: true,
        port: process.env.PORT || 3000,
        uiHost: 'http://localhost:3000',
        serverHost: "http://localhost:4000",
        version: 'v1',
    },
    database: {
        //mongo: process.env.MONGOLOCAL_URI,
        mongo: process.env.MONGOLOCAL_URI,
        defaultPageSize: 50,
    },
    app: {
        _id: "4be0516e-1c27-4ae0-b216-707a554a5803",
        token: ""
    }
};
//# sourceMappingURL=production.js.map