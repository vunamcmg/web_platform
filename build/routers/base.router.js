"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BaseRouter {
    onError(res, error) {
        console.log("err: ", error);
        res.send(error);
        // const options = error.options || error.errorInfo;
        res.redirect("/404");
    }
    onSuccess(res, object = {}, extras = {}) {
    }
    async validateJSON(body, schema) {
        // const validate = utilService.validateJSON(schema, body)
        // if (!validate.isValid) {
        //     throw errorService.router.requestDataInvalid(validate.message);
        // }
    }
    route(func) {
        return (req, res) => func
            .bind(this)(req, res)
            .catch((error) => {
            this.onError(res, error);
        });
    }
}
exports.BaseRouter = BaseRouter;
//# sourceMappingURL=base.router.js.map