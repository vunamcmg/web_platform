"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const view_router_1 = require("./view.router");
const api_router_1 = require("./api.router");
const view = (new view_router_1.ViewRouter()).router;
exports.view = view;
const api = (new api_router_1.ApiRouter()).router;
exports.api = api;
//# sourceMappingURL=index.js.map