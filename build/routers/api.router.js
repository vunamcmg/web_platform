"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_router_1 = require("./base.router");
const express = require("express");
const path = require("path");
const fs = require("fs");
const dirTree = require("directory-tree");
class ApiRouter extends base_router_1.BaseRouter {
    constructor() {
        super();
        this.router = express.Router();
        this.routing();
    }
    routing() {
        this.router.get("/setting", this.route(this.getSetting));
        this.router.post("/setting", this.route(this.updateSetting));
        this.router.get("/page", this.route(this.getPage));
        this.router.get("/page/raw", this.route(this.getPageRaw));
        this.router.post("/page/raw", this.route(this.updatePageRaw));
    }
    async getSetting(req, res) {
        const token = req.query.token;
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`));
        res.render(path.join(__dirname, `../../views/setting/setting`), { setting });
    }
    async updateSetting(req, res) {
        const token = req.query.token;
        fs.writeFileSync(path.join(__dirname, `../../views/setting/setting.json`), JSON.stringify(req.body));
        res.render(path.join(__dirname, `../../views/setting/setting`), { setting: req.body });
    }
    async getPage(req, res) {
        const token = req.query.token;
        const tree = dirTree(path.join(__dirname, "../../views"));
        res.status(200).json(tree);
    }
    async getPageRaw(req, res) {
        const token = req.query.token;
        const fileRaw = fs.readFileSync(req.query.path, "utf8");
        res.status(200).json({
            path: req.query.path,
            content: fileRaw
        });
    }
    async updatePageRaw(req, res) {
        const token = req.query.token;
        const content = req.body.content;
        console.log("content: ", req.body);
        // res.status(200).json({
        //     path: "q2121"
        // })
        const fileRaw = fs.writeFileSync(req.query.path, content);
        res.status(200).json({
            path: req.query.path,
            content: fileRaw
        });
    }
}
exports.ApiRouter = ApiRouter;
//# sourceMappingURL=api.router.js.map